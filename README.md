### 1. Performance optimization
[Performance optimization](/performance-optimization.md)

### 2. Twitter search web app
[Twitter search app](https://bitbucket.org/lm555/twitter-search-app)

### 3. CV
[Linkedin profile](https://www.linkedin.com/in/domen-zajc-73582227/)

I'm with Toshl Inc. for 4 years, working as a Software engineer on a [web application](https://toshl.com/connect/login/) written in Angular 1.
For the last 2 years I'm working on the project as a lead developer. 
I have experiences with the following technologies: D3.js, Node.js, Gulp, Boostrap, Sass, RxJs and of course Angular.

