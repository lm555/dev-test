1. In the first step I decided to combine two sequential bindings into one.  
[http://jsfiddle.net/cpa8cd8f/1/](http://jsfiddle.net/cpa8cd8f/1/)

2. In the second step I removed {{ }} data bindings and replaced them with ng-bind directive, because it places a watcher on the passed variable. Not like the {{}} curly braces which are dirty checked and refreshed in every $digest cycle, even if that is not necessary.  
[http://jsfiddle.net/cpa8cd8f/3/](http://jsfiddle.net/cpa8cd8f/3/)

3. In the third step I updated Angular from 1.1 to 1.6 so I can use all of the advantages a higher version of Angular provides. As listed below.  
[http://jsfiddle.net/cpa8cd8f/7/](http://jsfiddle.net/cpa8cd8f/7/)

4. In the fourth step I used track by index so when the value list is re-rendered Angular doesn't have to rebuild an element which has already been rendered. This speeds up the cycle and avoids useless DOM manipulation.  
[http://jsfiddle.net/cpa8cd8f/8/](http://jsfiddle.net/cpa8cd8f/8/)

5. In the fifth step I used one way data binding whenever two way data binding wasn't necessary.  
[http://jsfiddle.net/cpa8cd8f/9/](http://jsfiddle.net/cpa8cd8f/9/)

6. In the sixth step I used ng-if for the elements which don't have to be rendered on initialization, but only when the condition is met.  
[http://jsfiddle.net/cpa8cd8f/10/](http://jsfiddle.net/cpa8cd8f/10/)

7. In the seventh step I used ng-options instead of ng-repeat, because of the use benefits, such as reduced memory consumption by not creating a new scope for each repeated instance and increased render speed by creating the options in a documentFragment instead of individually.  
[http://jsfiddle.net/cpa8cd8f/12/](http://jsfiddle.net/cpa8cd8f/12/)

8. In the seventh step I decided to render the data visible to the users eye first and after that fill the hidden option values.  
[http://jsfiddle.net/cpa8cd8f/13/](http://jsfiddle.net/cpa8cd8f/13/)

9. In the eight step I decided to generate the html and bind it to the view with ng-bind-html.  
[http://jsfiddle.net/cpa8cd8f/14/](http://jsfiddle.net/cpa8cd8f/14/)


I think every use case scenario is not the same, therefore we should choose the most appropriate optimisation technics for it.

#### How many groups/filters can you load in under 1 sec?
≈50 groups/ 1500filters. 
